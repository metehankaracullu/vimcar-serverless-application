import { expect as expectCDK, matchTemplate, MatchStyle } from '@aws-cdk/assert';
import * as cdk from '@aws-cdk/core';
import * as VimcarServerlessApplication from '../lib/vimcar-serverless-application-stack';

test('Empty Stack', () => {
    const app = new cdk.App();
    // WHEN
    const stack = new VimcarServerlessApplication.VimcarServerlessApplicationStack(app, 'MyTestStack');
    // THEN
    expectCDK(stack).to(matchTemplate({
      "Resources": {}
    }, MatchStyle.EXACT))
});
