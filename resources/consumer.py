import lambdalogging
import boto3
import json
import config
import base64

LOG = lambdalogging.getLogger(__name__)
stream_name = config.STREAM_NAME
kinesis_client = boto3.client('kinesis', region_name='eu-central-1')


def lambda_handler(event, context) -> dict:
    for record in event['Records']:
        # Kinesis data is base64 encoded so decode here
        payload = base64.b64decode(record['kinesis']['data']).decode('utf-8')
        data = json.loads(payload)
        values = data['data']
        values = values.replace("[", "")
        values = values.replace("]", "")
        datalist = list(map(int, values.split(", ")))
        fizzbuzz = []
        for d in datalist:
            fizzbuzz.append(fizz_buzz(d))

        LOG.info("Transformed Data: " + str(fizzbuzz))


def fizz_buzz(num):
    if num % 3 == 0 and num % 5 == 0:
        return "fizzBuzz"
    elif num % 3 == 0:
        return "fizz"
    elif num % 5 == 0:
        return "buzz"
    else:
        return num
