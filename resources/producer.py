"""Producer function handler."""

import json
import time
from http import HTTPStatus

import boto3
from botocore.exceptions import ClientError

import config
import lambdalogging

LOG = lambdalogging.getLogger(__name__)
stream_name = config.STREAM_NAME
kinesis_client = boto3.client('kinesis', region_name='eu-central-1')


def lambda_handler(request: dict, _context) -> dict:
    if request and request["body"]:
        request_body = json.loads(request["body"])

        if "data" not in request_body:
            LOG.error(f"The field 'data' missing from request - {request_body['data']}")
            return response_build(HTTPStatus.BAD_REQUEST, request)

        LOG.info(f"New request received - {request_body['data']}")
        try:
            create_event(request_body)
        except ClientError as err:
            LOG.error(f"Error encountered during storing data - {request_body['data']}")
            return response_build(HTTPStatus.INTERNAL_SERVER_ERROR, err)

        LOG.info(f"Your data uploaded successfully - {request_body['data']}")
        response_message = {
            "status": HTTPStatus.OK,
            "message": "Your Data Uploaded Successfully",
        }
        return response_build(HTTPStatus.OK, response_message)


def response_build(status_code: int, body: dict) -> dict:
    return {
        "statusCode": status_code,
        "body": json.dumps(body),
        "isBase64Encoded": False,
        "headers": {
            "Content-Type": "application/json"
        }
    }


def create_event(request_body: dict):
    milliseconds = int(round(time.time() * 1000))
    try:
        return put_to_stream(request_body, milliseconds)
    except ClientError as err:
        raise err


def put_to_stream(request_body, timestamp):
    data = request_body['data']
    payload = {
        'data': str(data),
        'timestamp': str(timestamp),
    }

    return kinesis_client.put_record(
        StreamName=stream_name,
        PartitionKey='data',
        Data=json.dumps(payload))
