FROM docker

RUN apk add bash
RUN apk add --no-cache \
        python3 \
        py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install \
        awscli \
    && rm -rf /var/cache/apk/*

RUN apk add vim curl wget docker git
RUN apk add --update nodejs nodejs-npm
RUN npm install -g aws-cdk
