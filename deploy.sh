#!/bin/bash

if [ "$1" == diff ]; then
  cdk diff vimcar-stack
elif [ "$1" == deploy ]; then
  cdk deploy vimcar-stack --require-approval never
elif [ "$1" == destroy ]; then
  echo y | cdk destroy vimcar-stack
fi