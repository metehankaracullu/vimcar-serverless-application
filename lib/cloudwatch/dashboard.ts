import {Dashboard, Metric, Row, SingleValueWidget, Spacer, Statistic} from "@aws-cdk/aws-cloudwatch";
import {Construct, Duration} from "@aws-cdk/core";
import {Function} from '@aws-cdk/aws-lambda'
import {LambdaRestApi} from "@aws-cdk/aws-apigateway";
import {Stream} from "@aws-cdk/aws-kinesis";

export const createDashboard = (
    scope: Construct,
    producerLambda: Function,
    consumerLambda: Function,
    apiGW: LambdaRestApi,
    stream: Stream
) => {
    const producerLambdaInvocationMetric = producerLambda.metricInvocations({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Number of Invocations"
    });
    const producerLambdaDurationMetric = producerLambda.metricDuration({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Average Execution Duration"
    });
    const producerLambdaConcurrentExecutionsMetric = producerLambda.metric("ConcurrentExecutions", {
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Number of ConcurrentExecutions"
    });

    const producerLambdaWidget = new SingleValueWidget({
        metrics: [producerLambdaInvocationMetric, producerLambdaDurationMetric, producerLambdaConcurrentExecutionsMetric],
        height: 6,
        width: 12,
    });

    const consumerLambdaInvocationMetric = consumerLambda.metricInvocations({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Number of Invocations"
    });
    const consumerLambdaDurationMetric = consumerLambda.metricDuration({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Average Execution Duration"
    });
    const consumerLambdaConcurrentExecutionsMetric = consumerLambda.metric("ConcurrentExecutions", {
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Number of ConcurrentExecutions"
    });

    const consumerLambdaWidget = new SingleValueWidget({
        metrics: [consumerLambdaInvocationMetric, consumerLambdaDurationMetric, consumerLambdaConcurrentExecutionsMetric],
        height: 6,
        width: 12,
    });

    const dashboard = new Dashboard(scope, "vimcar-sls-metric", {
        dashboardName: "data-processing-dashboard",
    });

    const firstRow = new Row(producerLambdaWidget, consumerLambdaWidget);

    const apiGWRequestPerMinute = apiGW.metricCount({
        period: Duration.minutes(1),
        statistic: Statistic.SAMPLE_COUNT,
        label: "Number of Requests per minute"
    });

    const apiGWLatency = apiGW.metricLatency({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Latency of API GW"
    });

    const apiGW4xxCountMetric = new Metric({
        metricName: '4XXError',
        namespace: 'AWS/ApiGateway',
        statistic: Statistic.SUM,
        label: 'HTTP Code 4XX Count',
        region: 'eu-central-1',
        dimensions: {
            ApiName: apiGW.restApiName
        },
        period: Duration.minutes(1)
    });

    const apiGW5xxCountMetric = new Metric({
        metricName: '5XXError',
        namespace: 'AWS/ApiGateway',
        statistic: Statistic.SUM,
        label: 'HTTP Code 5XX Count',
        region: 'eu-central-1',
        dimensions: {
            ApiName: apiGW.restApiName
        },
        period: Duration.minutes(1)
    });

    const ApiGWWidget = new SingleValueWidget({
        metrics: [apiGWRequestPerMinute, apiGWLatency, apiGW4xxCountMetric, apiGW5xxCountMetric],
        height: 6,
        width: 24,
    });

    const secondRow = new Row(ApiGWWidget);


    const incomingBytesMetric = stream.metricIncomingBytes({
        period: Duration.minutes(1),
        statistic: Statistic.SUM,
        label: "Sum of Incoming bytes to Kinesis"
    })

    const getRecordsMetric = stream.metricGetRecords({
        period: Duration.minutes(1),
        statistic: Statistic.SUM,
        label: "Sum of get Records"
    })

    const incomingRecordsMetric = stream.metricIncomingRecords({
        period: Duration.minutes(1),
        statistic: Statistic.SUM,
        label: "Sum of Incoming Records"
    })

    const getRecordsLatencyMetric = stream.metricGetRecordsLatency({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Average of get Records Latency"
    })

    const putRecordLatencyMetric = stream.metricPutRecordLatency({
        period: Duration.minutes(1),
        statistic: Statistic.AVERAGE,
        label: "Average of put Records Latency"
    })

    const streamWidget = new SingleValueWidget({
        metrics: [incomingBytesMetric, getRecordsMetric, incomingRecordsMetric, getRecordsLatencyMetric, putRecordLatencyMetric],
        height: 6,
        width: 24,
    });

    const thirdRow = new Row(streamWidget);


    const spacer = new Spacer({
        width: 24,
        height: 1,
    });

    dashboard.addWidgets(firstRow, spacer, secondRow, spacer, thirdRow);
}

