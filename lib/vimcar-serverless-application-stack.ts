import * as cdk from '@aws-cdk/core';
import {Code, Function, Runtime, StartingPosition} from '@aws-cdk/aws-lambda'
import {Stream} from '@aws-cdk/aws-kinesis';
import {KinesisEventSource} from '@aws-cdk/aws-lambda-event-sources';
import * as path from "path";
import {createDashboard} from "./cloudwatch/dashboard";
import apigateway = require('@aws-cdk/aws-apigateway');

export class VimcarServerlessApplicationStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const veichlesDataKinesisStream = new Stream(this, "veichles-data-kinesis", {
            streamName: "veichles-data-kinesis",
            shardCount: 1
        });

        const producerLambda = new Function(this, 'producer-lambda', {
            code: Code.fromAsset(path.join(__dirname, '../resources/')),
            handler: "producer.lambda_handler",
            runtime: Runtime.PYTHON_3_8,
            memorySize: 256,
            environment: {
                'STREAM_NAME': veichlesDataKinesisStream.streamName,
            },
        });

        const vehiclesApi = new apigateway.LambdaRestApi(this, 'vehiclesApi', {
            handler: producerLambda,
            proxy: false
        });

        const items = vehiclesApi.root.addResource('data');
        items.addMethod('POST'); // POST /data

        veichlesDataKinesisStream.grantWrite(producerLambda)

        const consumerLambda = new Function(this, 'consumer-lambda', {
            code: Code.fromAsset(path.join(__dirname, '../resources/')),
            handler: "consumer.lambda_handler",
            runtime: Runtime.PYTHON_3_8,
            memorySize: 256,
            environment: {
                'STREAM_NAME': veichlesDataKinesisStream.streamName,
            },
        });

        consumerLambda.addEventSource(new KinesisEventSource(veichlesDataKinesisStream, {
            batchSize: 100,
            startingPosition: StartingPosition.TRIM_HORIZON
        }));

        createDashboard(this, producerLambda, consumerLambda, vehiclesApi, veichlesDataKinesisStream);

    }
}
