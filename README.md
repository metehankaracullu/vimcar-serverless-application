# Welcome to my Vimcar Infrastructure Challenge!

This is a serverless project that gets data from user (producer lambda), then processing it (consumer lambda).

![AWS - INFRASTRUCTURE](vimcar-diagram.png)

## CI - CD Stages:

`Register_image:`

We need a docker container that includes cdk binaries and aws cli for connecting our aws account.

Yes it is possible to find any image with these requirements, but if you don't want to get docker pull limit errors, You can generate your own container and register it to `gitlab container registry` directly. And this is more efficient way then pulling image from docker hub.

`Diff_stacks`, `Deploy_stacks`, `Destroy_stacks`

These stages are implemented for managing your cloudformation stacks.

Diff stage is checking that is your code has any difference between current cloudformation stack on AWS.

Deploy stage is adding/removing your resources if you have any differences.

Destroy stage is destroying all of your interested cloudformation stack.

### Source Code
resources folder

`Sample Request:`

curl --location --request POST 'https://xxpgaa1i90.execute-api.eu-central-1.amazonaws.com/prod/data' \
--header 'Content-Type: text/plain' \
--data-raw '{"data": [1, 5, 15, 3, 11, 10]}'

`Sample Transformed Data: ` [1, 'buzz', 'fizzBuzz', 'fizz', 11, 'buzz']

[consumer log groups](https://eu-central-1.console.aws.amazon.com/cloudwatch/home?region=eu-central-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fvimcar-stack-consumerlambdaFE5BF97E-TwzjqYvbOnqS)

[dashboard](https://eu-central-1.console.aws.amazon.com/cloudwatch/home?region=eu-central-1#dashboards:name=data-processing-dashboard)