#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { VimcarServerlessApplicationStack } from '../lib/vimcar-serverless-application-stack';

const app = new cdk.App();
new VimcarServerlessApplicationStack(app, 'VimcarServerlessApplicationStack');

const env = {
    account: '295896871796',
    region: 'eu-central-1'
}

new VimcarServerlessApplicationStack(app, "vimcar-stack", { env, tags: {
        Project: 'Vimcar-Serverless',
        Domain: 'FizzBuzz',
        ManagedBy: 'CDK-CF',
        Team: 'Mete Only',
    }});